var mongoose = require('mongoose');
var {
  Schema
} = mongoose;

var commentsSchema = new Schema(
{
  ebookId: { type: Schema.Types.ObjectId, ref: 'book' },
  user : {
    name :{ type: String},
    id:{ type: Number }
  },
  comment : {type:Object},
  comment_valoration : Number
},
{
  timestamps: true
}

);

module.exports = mongoose.model('comment', commentsSchema);