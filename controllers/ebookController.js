module.exports = {
    add,
    del,
    update,
    get,
    getToRead,
    firstScan
};

var fs = require('fs');
var EPub = require('epub2/node');
var path = require('path');
var jimp = require('jimp');
var Ebook = require(path.join('../models/Ebook'));
var bookEvent =require('./socket')
async function resizeImg(data) {
    img = await jimp.read(data);
    imgResized = await img.resize(100, 156).getBufferAsync(jimp.MIME_PNG)
    return imgResized;
}

async function add(ebookpath) {
    var ebook = new Ebook;
    await EPub.createAsync(ebookpath).then(async function (epub) {
        ebook.title = epub.metadata.title;
        ebook.author = epub.metadata.creator;
        ebook.path = ebookpath;
        ebook.folderPath = path.dirname(ebookpath);
        ebook.description = epub.metadata.description;
        epub.getImage(epub.metadata.cover, async function (err, data, mimeType) {
            ebook.covermimeType = mimeType;
            ebook.cover = await resizeImg(data);
            Ebook.findOne({
                'path': ebook.path
            }, (err, data) => {
                if (data == null) {
                    ebook.save();
                    bookEvent.newBook(ebook)
                    console.log(ebook.title + " has added");
                }
            });


        });

    });
    return ebook
}

async function del(ebookpath) {
   ebook = await Ebook.findOne({
        'path': ebookpath
    }) 
        console.log(ebook.title + " has removed");
        bookEvent.bookDeleted(ebook)
        ebook.remove();
        return ebook;
}

async function get() {
    ebooks = await Ebook.find();
    return ebooks;
}

async function getToRead(path) {
    var epub = await EPub.createAsync(path)
    return epub
}

async function update(ebookpath) {
    console.log(ebookpath);

}

async function firstScan() { //detects books deleted while server is off
    var ebooks = await get();
    ebooks.forEach(book => {
        try {
            var file = fs.readFileSync(book.path)
        } catch (err) {}
        if (!file) {
            del(book.path);
            console.log(book.title + ' was deleted while server was offline')
        }
    });
}



