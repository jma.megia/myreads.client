

var mongoose = require('mongoose');
var {
  Schema
} = mongoose;

var booksSchema = new Schema({
  title: String,
  description: String,
  author: String,
  covermimeType: String,
  path: String,
  folderPath: String,
  cover: Buffer,
  valoration:{type:Number, default: 0},
});

module.exports = mongoose.model('ebook', booksSchema);