const Comments = require('../models/Comment')

module.exports={
    avg:async  (bookId)=>{
        var comments=await Comments.aggregate([
            {    
                $group: { 
                    _id: "$ebookId",
                    media: { 
                        $avg: "$comment_valoration" 
                        } 
                     } 
            }     
    ])
    let comment =  comments.filter(comments=> comments._id == bookId)
    if(comment.length>0) res= Math.round(comment[0].media)
    else res=0
    return res
    }
}