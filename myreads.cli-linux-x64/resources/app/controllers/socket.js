var Ebook = require('../models/Ebook');
var Library = require('../models/Library');
var Reads = require('../models/Reads')
var Comments = require('../models/Comment')
var commentFilter= require('./commentController.js')
var {findFolders, deletedDir} = require('./fsController');
var {updateLibrary,findLibrary,deletedLib} = require('./libraryController')
var fs = require('fs');
var path=require('path')
var config=require('../config')
var {watching} = require('./scandirController')
const data = {
    serverId:null,
    token:null,
    serverName:config.SERVER_NAME,
}

var socket = require('socket.io-client')(config.SERVER_IP+':'+config.SERVER_PORT,{secure: config.SECURE, reconnect: true});
socket.on('connect', function () {
    console.log('getting auth');
    socket.emit('login', {
        body: {
            email: config.LOGIN_EMAIL,
            password: config.LOGIN_PASS,
            name: data.serverName,
            role: 'server'
        }
    })
});
socket.on('authRes', function (response) {
    if (response.body.auth) {
        console.log('Connected, serverId : '+response.serverId);
        data.token = response.body.token;
        data.serverId=response.serverId;
    } else{
        console.log('Auth failed');
    }
});

socket.on('disconnect', function () {
    //console.log('Connection closed');
});
socket.on('reqBooks', async (request) => {
    try {var reads=await Reads.find({userId:request.userId})}
    catch(err){
       // console.error(err);
    }
    try{var ebooks =  await Ebook.find({'folderPath': request.library.path})}
    catch(err){
       // console.error(err);
    }
    var resData={socketId:request.socketId, books:ebooks, library:request.library, reads:reads};
    var response=Object.assign(data,resData)

    //console.log('sending books of lib : '+request.library.name)
    socket.emit('resBooks', response);
})

socket.on('reqBook', async (request) => {
    var book = await fs.readFileSync(request.bookPath)
    var response={
        book:book,
        userId:request.userId,
        download:request.download,
        fileName:request.fileName,
        token:data.token,
        progress:request.progress,
        socketId:request.socketId
    }
    socket.emit('resBook', response)
})

socket.on('connect_timeout', (timeout) => {
    console.log(timeout + ' ...')
});

socket.on('reqFiles', async request=>{
  //  console.log('request files')
    files = await findFolders(request.folder)
    var resData = {files: files.files,dirs:files.dirs,userId: request.userId, socketId:request.socketId}
    var response=Object.assign(data,resData)
    socket.emit('resFiles',response)
})

socket.on('reqNewLibrary', async request=>{
    library = new Library({name:request.libraryName, path:path.join(config.BOOKS_ROOT_FOLDER,request.libraryPath),serverId:data.serverId});
    var resData =  {userId:data.userId,error:null,library:null,}
    var response=Object.assign(data,resData)
    library.save((error,library)=>{
        console.log(library.name)
        if(error) response.error=error
        else response.library=library
        watching(path.join(library.path))
        socket.emit('resNewLibrary',response)
    })
})
socket.on('reqLibraries', async request=>{
    //console.log(request.server.allowed_server)
     if(request.server.allowed_server!=undefined){
        libraries=await Library.find({_id : {"$in":JSON.parse(request.server.allowed_server.libraries)}})
     } else libraries=await Library.find()
     libraries.forEach(library => {
         library.serverName=data.serverName
     });
    var resData =  {
        libraries:libraries,
        userId:request.userId,
        socketId:request.socketId
    }
    var response=Object.assign(data,resData)
    socket.emit('resLibraries',response)

}),
socket.on('reqDelLibrary', async request=>{
    var isDeleted=await deletedLib(request.library);
    var resData =  {userId:request.userId,library:isDeleted,serverId:request.serverId}
    var response=Object.assign(data,resData)
    socket.emit('resDelLibrary',response)
})

socket.on('reqRenameLibrary', async request=>{
    var isRenamed=await updateLibrary(request.library);
    var resData =  {userId:request.userId,library:isRenamed}
    var response=Object.assign(data,resData)
    socket.emit('resRenameLibrary',response)
})

socket.on('reqBookComment',async request=>{
   // console.log(request.bookId)
try{
var alreadyExistis = await Comments.findOne({ebookId:request.bookId, user:{name:request.user.name, id:request.user.id}})
}catch{}
if(!alreadyExistis){
    try{
        var post=await Comments.create({
            ebookId:request.bookId,
            user :{name:request.user.name, id:request.user.id},
            comment:request.post.comment,
            comment_valoration:request.post.valoration
        })
        var bookValoration=await commentFilter.avg(request.bookId)
        let ebook=await Ebook.findOne({_id:request.bookId})
        ebook.valoration=bookValoration
        ebook.save()
    }catch(e){console.log(e)}
}else   bookValoration=null
    var resData =  {confirmation:{bookId:request.bookId,post:post,newValoration:bookValoration,userId:request.user.id}}
    var response=Object.assign(data,resData)
    socket.emit('resBookComment',response)
})
socket.on('reqComments', async request=>{
    var comments=await Comments.find({ebookId:request.bookId})
    var resData =  {comments:comments,userId:request.userId}
    var response=Object.assign(data,resData)
    socket.emit('resComments',response)
})
 
socket.on('reqSetReaded',async request=>{
    read=await Reads.findOne({ebookId:request.bookId, userId : request.userId})
    if(read) {
        read.readed=request.percent
        read.save()
    }
    else Reads.create({ebookId:request.bookId, userId : request.userId, readed:request.percent })
})

socket.on('reqDeleteValoration',async request=>{
    try{
        let comment= await Comments.findOne({_id:request.commentId})
        await comment.remove()
        var bookValoration=await commentFilter.avg(request.bookId)
        let ebook=await Ebook.findOne({_id:request.bookId})
        ebook.valoration=bookValoration
        ebook.save()
    }catch(e){console.log(e)}
    var resData =  {confirmation:{bookId:request.bookId,newValoration:bookValoration,userId:request.user.id}}
    var response=Object.assign(data,resData)
    socket.emit('resDeleteValoration',response)
})

    module.exports= {
        newBook :async (newBook)=>{
            var libraries=await findLibrary(newBook)
            if(libraries.length>0){
                //console.log(newBook.title)
                var resData = {newBook:newBook,libraries:libraries}
                var request=Object.assign(data,resData)
                socket.emit('newBook',request)
            }    
        },
        bookDeleted:async (delBook)=>{
           
            var libraries=await findLibrary(delBook)
            if(libraries.length>0){
                var resData = {book:delBook,libraries:libraries}
                var request=Object.assign(data,resData)    
                socket.emit('bookDeleted',request)
            }    
        }
    }
    