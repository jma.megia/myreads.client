var Ebook = require('../models/Ebook')
var Library=require('../models/Library')

module.exports={
    updateLibrary : async function(lib){
        library=await Library.findOneAndUpdate({path:lib.path},lib,{ new: true })
    },
    findLibrary : async function(newBook){
        var libraries=[]
        try{
            libraries=await Library.find({path:newBook.folderPath})
        }
        catch{  
        }
        return libraries
    },
     deletedLib : async function(lib){
        var library = await Library.findOne({
            '_id': lib._id
        })
        ebooks= await Ebook.deleteMany({folderPath:library.path})
        var result=null;
        if(library) {
            try{
            result= await library.remove()
            } catch{library=null}
        } 
        return result;
    }
    
}
