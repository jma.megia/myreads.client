var fs = require('fs')
var path = require('path')
var config = require('../config')
var {
    join
} = require('path')


 async function findFolders(folder) {
     
   folder = path.join(config.BOOKS_ROOT_FOLDER,folder);
   console.log(folder)
   try{
        var files = fs.readdirSync(folder)
        var data ={
            dirs : [],
            files : [],
        }
        var stat;
        files.forEach(async file => {
            stat = await fs.statSync(join(folder, file))
            if (stat.isDirectory()) data.dirs.push(file)
            else data.files.push(file)
        })
        return data
    }catch(err){
        console.log(err)
        return err
    }
}


module.exports ={
    findFolders,
}