var mongoose = require('mongoose');
var {
  Schema
} = mongoose;

var librariesSchema= new Schema({
    name : {type :String, unique:true, required:true, dropDups: true},
    serverId : Number,
    path : String
})

module.exports = mongoose.model('library', librariesSchema);