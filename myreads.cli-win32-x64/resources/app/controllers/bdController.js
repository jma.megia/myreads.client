var config = require('../config')
var ebooks= require('./ebookController')
var mongoose=require('mongoose');
var dburl='mongodb://'+config.DB_HOST
var dboptions = {dbName:config.DB,useNewUrlParser: true}
async function toConect (){
    await mongoose.connect(dburl,dboptions)
        .then(db=>console.log('bd connected'),
        //mongoose.connection.dropDatabase()
        ebooks.firstScan()
        )
        .catch(err=>console.log(err));
}

module.exports={toConect}