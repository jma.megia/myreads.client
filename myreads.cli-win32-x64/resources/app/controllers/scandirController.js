module.exports = {
    initwatch,
    watching
};

var chokidar = require('chokidar');
var path = require('path')
var ebookcontroller = require('./ebookController')
var {deletedDir} = require('./fsController')
var config = require('../config')
var Library = require('../models/Library')

//var booksdir =path.join(config.BOOKS_ROOT_FOLDER);

async function initwatch(){
    libraries=await Library.find()
    libraries.forEach(library => {
        watching(library.path)
    });
}

function watching(booksdir) {
    console.log('watching : ' + booksdir)
    var watcher = chokidar.watch(booksdir, {
        ignored: /^\./,
        persistent: true,
        depth :0
    });
    watcher
        .on('add',async function (data) {
            if(isEpub(data)){
               ebook = await ebookcontroller.add(data);
            }
        })
        .on('unlink',async function (data) {
            if(isEpub(data)){
               ebook=await ebookcontroller.del(data);
            }
        })
        .on('change', (data) => {
            if(isEpub(data)){
            }
        })
        .on('unlinkDir', function(path){
                deletedDir(path)
        })
}
function isEpub(data) {
    if(path.extname(data)=='.epub'){
        return true
    }
}
