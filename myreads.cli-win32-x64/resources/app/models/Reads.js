var mongoose = require('mongoose');
var {
  Schema
} = mongoose;

var readsSchema = new Schema(
{
  ebookId: { type: Schema.Types.ObjectId, ref: 'book' },
  userId : Number,
  readed : {type : Number, default : 0}
},
{
  timestamps: true
}

);

module.exports = mongoose.model('reads', readsSchema);